process.env.TZ = 'UTC';

const fs = require ( 'fs' );

const Koa = require('koa');
const app = module.exports = new Koa();
const TRouter = require ( 'koa-router' );
const g_pRouter = new TRouter ();
const bodyParser = require('koa-body')();

const pg = require('knex')({
	client: 'pg',
	version: '7.2',
	connection: {
		host : '127.0.0.1',
		user : 'postgres',
		password : 'postgres',
		database : 'postgres'
	}
});



const TLessons = require ( "./logics/lessons.js" );



const g_pLessons = new TLessons ( pg );



// app.use(async function(ctx) {
// 	let pRequest = {};

// 	console.log ( ctx.request.query );
// 	ctx.set('Content-Type', 'application/json');
// 	g_pLessons.ParseGetParameters ( pRequest, ctx, ctx.request.query );
// 	// ctx.body = 'Hello World';
// 	ctx.body = await g_pLessons.GetLessonsInformation ( pRequest );
// });

g_pRouter.get ( '/', async (ctx, next) => {
	let pRequest = {};

	console.log ( ctx.request.query );
	ctx.set('Content-Type', 'application/json');
	g_pLessons.ParseGetParameters ( pRequest, ctx, ctx.request.query );
	// ctx.body = 'Hello World';
	ctx.body = await g_pLessons.GetLessonsInformation ( pRequest );
});

g_pRouter.get ( '/lessons', async (ctx, next) => {
	// let pRequest = {};

	// console.log ( ctx.request.query );
	// ctx.set('Content-Type', 'application/json');
	// g_pLessons.ParseGetParameters ( pRequest, ctx, ctx.request.query );
	// ctx.body = await g_pLessons.GetLessonsInformation ( pRequest );

	ctx.body = 'Adding lessons to data base.';
});
g_pRouter.post('/lessons', bodyParser, async (ctx, next) => {
	// function *(next){
	// console.log('\n------ post:/game/questions ------');
	console.log(ctx.request.body);
	// this.status = 200;
	// this.body = 'some jade output for post requests';
	// yield(next);

	// ctx.body = 'Adding lessons to data base.';
	ctx.body = await g_pLessons.ProcessAddLessonsRequest ( ctx, ctx.request.body );
});

g_pRouter.get ( '/add_lessons_form', async (ctx, next) => {
    ctx.type = 'html';
    ctx.body = fs.createReadStream('./static_files/add_lessons_form.html');
});
g_pRouter.get ( '/static_files/scripts.js', async (ctx, next) => {
    ctx.type = 'html';
    ctx.body = fs.createReadStream('./static_files/scripts.js');
});

app.use(g_pRouter.routes());
app.use(g_pRouter.allowedMethods());

// app.use(function* index() {
//   yield send(this, __dirname + '/index.html');
// });

app.on('error', (err, ctx) => {
	console.error('server error', err, ctx);
	// pg.destroy();
});



function cleanup() {
    console.log('Closing...');
    pg.destroy();
    process.exit(1);
}

process.on('SIGINT', cleanup);
process.on('SIGTERM', cleanup);



if (!module.parent) app.listen(3000);
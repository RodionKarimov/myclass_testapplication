const g_pUtilities = require ( "../base/utilities.js" );



class TLessons {
	constructor ( _pPg ) {
		this.m_pPg = _pPg;
	}

	/*
	  Here we should check user input for correctness.
	*/
	ParseGetParameters ( _pRequest, _pCtx, _aQuery ) {
		var date = new Date();
		console.log ( date.toISOString () );

		//---Checking correctness of date parameter.-------------------------------
		_pRequest.m_sDate1 = "1970-01-01";
		date.setFullYear ( date.getFullYear () + 1 );
		_pRequest.m_sDate2 = date.toISOString ();

		if ( _aQuery.date !== undefined ) {
			if ( g_pUtilities.isDate ( _aQuery.date ) ) {
				console.log ( "ParseGetParameters () : Date parameter is a date." );
				date = new Date ( _aQuery.date );

				_pRequest.m_sDate1 = _aQuery.date;

				date.setDate ( date.getDate () + 1 );
				_pRequest.m_sDate2 = date.toISOString ();

			} else {
				console.log ( "ParseGetParameters () : Date parameter is not a date." );

				let aDates = _aQuery.date.split ( "," );
				if ( ! aDates.length || ( aDates.length != 2 ) ||
				     ! g_pUtilities.isDate ( aDates [ 0 ] ) || ! g_pUtilities.isDate ( aDates [ 1 ] )
				   ) {
					_pCtx.throw ( 400, 'Error : dates parameter is in wrong format.' );
					return;
				}

				_pRequest.m_sDate1 = aDates [ 0 ];
				_pRequest.m_sDate2 = aDates [ 1 ];
				
			} //-else

		} //-if

		//---Checking status parameter.--------------------------------------
		_pRequest.m_iStatus = -1;
		if ( ( _aQuery.status !== undefined ) ) {
			if ( ( _aQuery.status !== "0" ) && ( _aQuery.status !== "1" ) ) {
				_pCtx.throw ( 400, 'Error : status parameter is in wrong format.' );
				return;
			}

			_pRequest.m_iStatus = Number ( _aQuery.status );
		} //-if

		//---Checking teacherIds parameter.---------------------------------
		// _pRequest.m_sTeachersIds = "";
		_pRequest.m_aTeachersIds = null;
		if ( ( _aQuery.teacherIds !== undefined ) ) {
			let aTeachersIds = _aQuery.teacherIds.split ( "," );
			let i = 0;
			let l = aTeachersIds.length;

			if ( aTeachersIds.length === 0 ) {
				_pCtx.throw ( 400, 'Error : teacherIds parameter is in wrong format.' );
				return;
			} //-if

			for ( ; i < l; i ++ ) {
				if ( ! Number.isInteger ( Number ( aTeachersIds [ i ] ) ) ) {
					_pCtx.throw ( 400, 'Error : teacherIds parameter is in wrong format.' );
					return;
				} //-if
			} //-for

			// _pRequest.m_sTeachersIds = _aQuery.teacherIds;
			_pRequest.m_aTeachersIds = aTeachersIds;

		} //-if

		//---Checking studentsCount parameter.------------------------------
		_pRequest.m_iStudentsCount1 = 0;
		_pRequest.m_iStudentsCount2 = 50000000000000;
		if ( ( _aQuery.studentsCount !== undefined ) ) {
			let aStudentsCounts = _aQuery.studentsCount.split ( "," );
			if ( aStudentsCounts.length === 1 ) {
				if ( ! Number.isInteger ( Number ( aStudentsCounts [ 0 ] ) ) ) {
					_pCtx.throw ( 400, 'Error : studentsCount parameter is in wrong format.' );
					return;
				} //-if

				_pRequest.m_iStudentsCount1 = Number ( aStudentsCounts [ 0 ] );
				_pRequest.m_iStudentsCount2 = Number ( aStudentsCounts [ 0 ] );

			} else if ( aStudentsCounts.length === 2 ) {
				if ( ! Number.isInteger ( Number ( aStudentsCounts [ 0 ] ) ) || ! Number.isInteger ( Number ( aStudentsCounts [ 1 ] ) ) ) {
					_pCtx.throw ( 400, 'Error : studentsCount parameter is in wrong format.' );
					return;
				} //-if

				_pRequest.m_iStudentsCount1 = Number ( aStudentsCounts [ 0 ] );
				_pRequest.m_iStudentsCount2 = Number ( aStudentsCounts [ 1 ] );

			} else {
				_pCtx.throw ( 400,'Error : studentsCount parameter is in wrong format.' );
				return;
			} //-if

		} //-if

		//---Checking lessonsPerPage parameter.-----------------------------
		_pRequest.m_iLessonsPerPage = 5000000000;
		if ( ( _aQuery.lessonsPerPage !== undefined ) ) {
			if ( ! Number.isInteger ( Number ( _aQuery.lessonsPerPage ) ) ) {
				_pCtx.throw ( 400, 'Error : lessonsPerPage parameter is in wrong format.' );
				return;
			} //-if

			_pRequest.m_iLessonsPerPage = Number ( _aQuery.lessonsPerPage );
		} //-if

		//---Checking page parameter.---------------------------------------
		_pRequest.m_iPage = 1;
		if ( ( _aQuery.page !== undefined ) ) {
			if ( ! Number.isInteger ( Number ( _aQuery.page ) ) ) {
				_pCtx.throw ( 400, 'Error : page parameter is in wrong format.' );
				return;
			} //-if

			if ( _aQuery.lessonsPerPage === undefined )
				_pRequest.m_iLessonsPerPage = 5;
			_pRequest.m_iPage = Number ( _aQuery.page );
		} //-if

		console.log ( "ParseGetParameters () :\n", _pRequest , "\n." );

	}



	async GetLessonsInformation ( _pRequest ) {  // _sResult
		let aRows = null;
		let aResults = [];
		let aResults2 = [];
		let pResult = null;
		let pQuery = this.m_pPg.from('lessons').select("*")
			.where ( "date", ">=", _pRequest.m_sDate1 )
			.where ( "date", "<=", _pRequest.m_sDate2 );
		let pLessonQuery = null;

		if ( _pRequest.m_iStatus >= 0 )
			pQuery.where ( "status", "=", _pRequest.m_iStatus );

		aRows = await pQuery;

		// console.log ( "GetLessonsInformation () : 1 : ", aRows, " ." );
			
		let aLessonTeachers = null;
		let aLessonStudents = null;
		let i = 0;
		let l = aRows.length;
		let j = 0;
		let l2 = 0;

		//---Obtaining teachers and students informtation for lessons.
		for ( ; i < l; i ++ ) {
			// console.log(`${row['id']} ${row['name']} ${row['price']}`);
			// console.log ( rows [ i ] );

			pResult = aRows [ i ];
			// aResults.push ( aRows [ i ] );

			pLessonQuery = this.m_pPg.from('lesson_teachers')
				// .select("*")
				.select("id", "name")
				.innerJoin('teachers', 'lesson_teachers.teacher_id', 'teachers.id')
				.where ( "lesson_teachers.lesson_id", "=", pResult.id );

			if ( _pRequest.m_aTeachersIds !== null )
				pLessonQuery.where ( "lesson_teachers.teacher_id", "in", _pRequest.m_aTeachersIds );

			aLessonTeachers = await pLessonQuery;

			if ( aLessonTeachers.length === 0 ) {
				continue;
			} //-if

			// console.log ( aLessonTeachers );

			// aResults [ aResults.length - 1 ].teachers = aLessonTeachers;
			// aRows [ i ].teachers = aLessonTeachers;
			pResult.teachers = aLessonTeachers;

			aLessonStudents = await this.m_pPg.from('lesson_students')
				// .select("*")
				.select("id", "name", "visit")
				.innerJoin('students', 'lesson_students.student_id', 'students.id')
				.where ( "lesson_students.lesson_id", "=", aRows [ i ].id )
				;
			// console.log ( aLessonStudents );

			if ( ( aLessonStudents.length === undefined ) ||
			     ( aLessonStudents.length < _pRequest.m_iStudentsCount1 ) ||
			     ( aLessonStudents.length > _pRequest.m_iStudentsCount2 ) ) {
				continue;
			} //-if

			// aRows [ i ].visitCount = 0;
			// aResults [ aResults.length - 1 ].visitCount = 0;
			pResult.visitCount = 0;
			for ( j = 0, l2 = aLessonStudents.length; j < l2; j ++ ) {
				if ( aLessonStudents [ j ].visit ) {
					// aRows [ i ].visitCount = aRows [ i ].visitCount + 1;
					pResult.visitCount = pResult.visitCount + 1;
				}
			} //-for

			// aResults [ aResults.length - 1 ].students = aLessonStudents;
			// aRows [ i ].students = aLessonStudents;
			pResult.students = aLessonStudents;

			aResults.push ( pResult );
		} //-for



		//---Selecting lessons for page.------------------------------
		i = ( _pRequest.m_iPage - 1 ) * _pRequest.m_iLessonsPerPage;
		l = ( _pRequest.m_iPage ) * _pRequest.m_iLessonsPerPage <= aResults.length ? ( _pRequest.m_iPage ) * _pRequest.m_iLessonsPerPage : aResults.length;

		for ( ; i < l; i ++ ) {
			// console.log(`${row['id']} ${row['name']} ${row['price']}`);
			// console.log ( rows [ i ] );

			aResults2.push ( aResults [ i ] );
		} //-for


		// console.log ( "GetLessonsInformation () : 2 : ", aResults, " ." );

		return JSON.stringify ( aResults2 );
	}



	async ProcessAddLessonsRequest ( _pCtx, _pRequest ) {
		let pParameters = JSON.parse ( _pRequest.value );

		const iTimeAmountInOneDay = 24 * 60 * 60 * 1000;
		const setAllowedDays = new Set ( [ "0", "1", "2", "3", "4", "5", "6" ] );
		// const mapDaysOffsets = new Map ( [ "0", 7 ], [ "1", 1 ], "2", "3", "4", "5", "6" ] );

		let dateFirst = null;
		let dateFirstPlusOneYear = null;
		let dateRunning = null;
		let dateLast = null;
		let dateBeginningOfAFirstWeek = null;
		let iLessonsCount = 0;

		let pQuery = null;

		let aResults = [];
		let aLessonInformation = null;

		let iWeekNumber = 0;
		let i = 0;
		let j = 0;
		let r = 0;
		let l = 0;
		let l2 = 0;
		let l3 = 0;

		console.log ( "TLessons.ProcessAddLessonsRequest () : ", pParameters, " ." );

		//---Checking whether specified teachersIds exist in data base.----------

		if ( ! pParameters.teachersIds.length ) {
			_pCtx.throw ( 400, 'Error : teachersIds parameter is in wrong format.' );
			return;
		} //-if

		// SELECT TOP 1 products.id FROM products WHERE products.id = ?;
		for ( i = 0, l = pParameters.teachersIds.length; i < l; i ++ ) {
			pQuery = this.m_pPg.from ( 'teachers' ).select ( "id" )
				.where ( "id", "=", pParameters.teachersIds [ i ] );
			// If length is undefined or equals to zero.
			if ( ! ( await pQuery ).length ) {
				_pCtx.throw ( 400, 'Error : teachersIds parameter is in wrong format.' );
				return;
			} //-if
		} //-for

		//---Checking firstDate parameter.---------------------------------------
		if ( ! pParameters.firstDate || ! g_pUtilities.isDate ( pParameters.firstDate ) ) {
			_pCtx.throw ( 400, 'Error : firstDate parameter is in wrong format.' );
			return;
		} //-if

		dateFirst = new Date ( pParameters.firstDate );
		dateFirstPlusOneYear = new Date ( pParameters.firstDate );
		dateFirstPlusOneYear.setFullYear ( dateFirstPlusOneYear.getFullYear () + 1 );;

		//---Checking days parameter.--------------------------------------------
		/*
		  If days days.length property is undefined or equals to zero or
		 there are duplicate days of week in array.
		*/
		if ( ! pParameters.days.length || [ ... new Set ( pParameters.days ) ].length != pParameters.days.length ) {
			_pCtx.throw ( 400, 'Error : days parameter is in wrong format.' );
			return;
		} //-if

		for ( i = 0, l = pParameters.days.length; i < l; i ++ ) {
			if ( ! setAllowedDays.has ( pParameters.days [ i ] ) ) {
				_pCtx.throw ( 400, 'Error : days parameter is in wrong format.' );
				return;
			} //-if

			pParameters.days [ i ] = Number ( pParameters.days [ i ] );
		} //-for

		//---Checking lessonsCount parameter.------------------------------------
		iLessonsCount = Number ( pParameters.lessonsCount );
		if ( pParameters.lessonsCount && ! Number.isInteger ( iLessonsCount ) ) {
			_pCtx.throw ( 400, 'Error : lessonsCount parameter is in wrong format.' );
			return;
		} //-if

		//---Checking lastDate parameter.----------------------------------------
		if ( pParameters.lastDate && ! g_pUtilities.isDate ( pParameters.lastDate ) ) {
			_pCtx.throw ( 400, 'Error : lastDate parameter is in wrong format.' );
			return;
		} //-if

		if ( ! pParameters.lessonsCount && ! pParameters.lastDate ) {
			_pCtx.throw ( 400, 'Error : You should specify lessonsCount or lastDate parameter.' );
			return;
		} //-if



		/*
		  If there is lessonsCount parameter, then we do not account for lastDate parameter.
		 If there is no it and there is lastDate parameter, then we set lessonsCount to very big number and
		 then just look for that we create less than 300 lessons and date of lesson is not farther, than one
		 year from first date.
		*/
		if ( ! pParameters.lessonsCount ) {
			if ( pParameters.lastDate ) {
				dateLast = new Date ( pParameters.lastDate );
				dateLast = new Date ( Math.min ( dateLast, dateFirstPlusOneYear ) );
				pParameters.lessonsCount = 50000000000;
			} //-if
		} else {
			dateLast = dateFirstPlusOneYear;
		} //-else

		pParameters.lessonsCount = Math.min ( pParameters.lessonsCount, 300 );

		// dateBeginningOfAFirstWeek = new Date ( dateFirst - dateFirst.getDay () * iTimeAmountInOneDay );
		dateBeginningOfAFirstWeek = new Date ( dateFirst );
		dateBeginningOfAFirstWeek.setDate ( dateBeginningOfAFirstWeek.getDate () - dateFirst.getDay () );

		for ( j = 0, l2 = pParameters.lessonsCount, iWeekNumber = 0;
		      ( j < l2 ) && ( dateRunning < dateLast ) && ( dateRunning < dateFirstPlusOneYear );
		      iWeekNumber ++ ) {
			for ( r = 0, l3 = pParameters.days.length;
				  ( r < l3 ) && ( j < l2 ) && ( dateRunning < dateLast ) && ( dateRunning < dateFirstPlusOneYear );
				  r ++ ) {
				/*
				  If this is a first week, and specified days are before firstDate.
				*/
				if ( ( j === 0 ) && ( pParameters.days [ r ] < dateFirst.getDay () ) )
					continue;

				// Beginning of a first week + week offset + day of a week.
				// dateRunning = new Date ( dateBeginningOfAFirstWeek
				//                          + j * 7 * iTimeAmountInOneDay
				//                          + pParameters.days [ r ] * iTimeAmountInOneDay
				//                        );
				dateRunning = new Date ( dateBeginningOfAFirstWeek );
				dateRunning.setDate ( dateRunning.getDate () + iWeekNumber * 7 + pParameters.days [ r ] );

				// console.log ( "TLessons.ProcessAddLessonsRequest ()"
				// 	, " :\n iWeekNumber : ", iWeekNumber
				// 	, " ;\n j : ", j, " , l2 : ", l2
				// 	, " ;\n r : ", r, " , l3 : ", l3
				// 	, " ;\n", dateFirst.getDay (), " , ", pParameters.days, " , ", pParameters.days [ r ]
				// 	, " -\n", dateBeginningOfAFirstWeek, " , ", dateRunning, " - ", dateLast, " , ", dateFirstPlusOneYear
				// 	, " ." );

				pQuery = this.m_pPg ( "lessons" )
					.returning ( [ "id", "date", "title", "status" ] )          // [ "id", "date", "title", "status" ]
					.insert ( {
						date : dateRunning.toISOString (),         // pParameters.firstDate
						title : pParameters.title,
						status : 0
						} );

				// console.log ( "TLessons.ProcessAddLessonsRequest () : ", pQuery.toString (), " ." );

				aLessonInformation = await pQuery;
				// console.log ( "TLessons.ProcessAddLessonsRequest () : ", aLessonInformation, " ." );

				aResults.push ( aLessonInformation [ 0 ].id );

				// console.log ( aResults );

				// console.log ( await this.m_pPg.from ( 'lessons' ).select ( "*" ) );

				if ( aResults.length > 0 ) {
					for ( i = 0, l = pParameters.teachersIds.length; i < l; i ++ ) {
						await this.m_pPg ( "lesson_teachers" )
						.insert ( {
							lesson_id : aResults [ aResults.length - 1 ],
							teacher_id : pParameters.teachersIds [ i ]
							} );
					} //-for
				} //-if

				// console.log ( await this.m_pPg.from ( 'lesson_teachers' ).select ( "*" ) );

				j = j + 1;

			} //-for

		} //-for



		return JSON.stringify ( aResults );
	}

	async ParseAddLessonsParameters ( _pRequest, _pCtx, _aQuery ) {

	}

}



module.exports = TLessons;